import React from 'react';
import styles from './Home.module.css';
import Header from '../../components/Header/Header';
import Instaslider from '../../components/Instaslider/Instaslider';

export default function Home() {
  return (
    <div className={styles.Home__Container}>
        <Header />
        <div className={styles.Home__Wrapper}>
            <h1>Instaslider</h1>
            <Instaslider />
        </div>
    </div>
  );
}
