import React, {useState} from 'react';
import styles from './InstaLookup.module.css';

export default function InstaLookup({search}) {
  const [input, setInput] = useState('');
  return (
    <div className={styles.LookupContainer}>
        <input type="text" onChange={e => setInput(e.target.value)} value={input}/>
        <button onClick={() => {
          search(input)}}>Search IG</button>
    </div>
  );
}
