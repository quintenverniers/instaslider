import React from 'react';
import { Link } from "react-router-dom";
import styles from './Header.module.css';

export default function Header() {
  return (
    <div className={styles.Container}>
        <div className={styles.Wrapper}>
            <ul>
                <li>
                    <Link to="/">Home</Link>
                </li>
                <li>
                    <Link to="/login">Login</Link>
                </li>
            </ul>
        </div>
    </div>
  );
}
