import React, { useState, useEffect } from 'react';
import styles from './Instaslider.module.css';
import InstaLookup from '../InstaLookup/InstaLookup';
import axios from 'axios';

export default function Instaslider() {
    const [posts, setPosts] = useState([]);
    const [user, setUser] = useState('jamieleesix');
    const [postsIndex, setPostIndex] = useState(0);
    const [isLoading, setLoading] = useState(true);
    const [hasError, setError] = useState(false);

    useEffect(() => {   
        setError(false); 
        axios.get(`https://www.instagram.com/`+user+`/?__a=1`)
        .then(res => {
            const persons = res.data.graphql.user.edge_owner_to_timeline_media.edges;
            console.log(persons);
            if(persons.length > 0) {
                setPosts(persons);
                setLoading(false);
            } else {
                setError(true);
                setLoading(false);
            }
        }).catch(err => console.log(err));
    }, [user]);

    function NextSlide() {
        //increase postIndex
        console.log("next");
        if(postsIndex < posts.length-1) {
            setPostIndex(postsIndex+1);
        } else {
            setPostIndex(0);
        }
        
    }

    function PrevSlide() {
        console.log("prev");
        if(postsIndex > 0) {
            setPostIndex(postsIndex-1);
        } else if(postsIndex === 0) {
            console.log(posts.length);
            setPostIndex(posts.length -1);
        }
    }

    const handleInputChange = (input) => {
        setUser(input);
    }

    return (
        <div className={styles.Slider__Container}>
            {isLoading ? (<div>Loading ...</div>) : 
                (<div>
                    <InstaLookup search={handleInputChange}/>
                    <div className={styles.Slider__Wrapper}>
                        {hasError ? (<div>Sorry this user doesn't exist. Please try again.</div>) : 
                        (<div className={styles.Slider__Wrapper}>
                            <div className={styles.Button} onClick={() => PrevSlide()}>
                                <p>Prev</p>
                            </div>
                            <div className={styles.Image}>
                                <p>{posts[postsIndex].node.edge_media_to_caption.edges[0].node.text}</p>
                                <img src={posts[postsIndex].node.display_url} alt="afbeelding"/>
                            </div>
                            <div className={styles.Button} onClick={() => NextSlide()}>
                                <p>Next</p>
                            </div>
                        </div>)}
                    </div>
                </div>)
            }   
        </div>
    );
}
